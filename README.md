# TacJS

Comply to the European cookie law using [tarteaucitron.js](https://github.com/AmauriC/tarteaucitron.js).

## Installation

1. Download and enable [TacJS module](https://www.drupal.org/project/tacjs).

2. Configure at Administration > Configuration System > TacJS settings (requires administer tacjs configuration permission).

__As the module allows adding unfiltered text to the settings, only grant the 'Administer TacJS' permission to trusted users.__

## Features

- Manage dialog
- Add services
- Edit texts
- Store proof of consent

## Contributing

Everyone is more than welcome to contribute to this little module!
To compress lang files, run `npm install` and `npm run uglify`.
