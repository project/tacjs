<?php

namespace Drupal\tacjs_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter;

/**
 * Plugin implementation of the 'oembed' formatter integrated with TacJS.
 *
 * @FieldFormatter(
 *   id = "tacjs_oembed",
 *   label = @Translation("oEmbed content (TacJS integration)"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class TacJSOEmbedFormatter extends OEmbedFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      if (!isset($element[$delta]['#tag']) || $element[$delta]['#tag'] !== 'iframe') {
        continue;
      }

      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};
      $max_width = $this->getSetting('max_width');
      $max_height = $this->getSetting('max_height');
      $resource_url = $this->urlResolver->getResourceUrl($value, $max_width, $max_height);
      $resource = $this->resourceFetcher->fetchResource($resource_url);
      $provider_name = mb_strtolower($resource->getProvider()->getName());

      $element[$delta]['#tag'] = 'div';
      $element[$delta]['#attributes']['class'][] = $provider_name . '_player';

      switch ($provider_name) {
        case 'youtube':
          unset($element[$delta]['#attributes']['src']);
          // @see https://blog.devgenius.io/ad5e2d1049a
          $regexp = '/^.*(youtu.*be.*)\/(watch\?v=|embed\/|v|shorts|)(.*?((?=[&#?])|$)).*/';
          preg_match($regexp, $value, $matches);
          if (!empty($matches[3])) {
            $element[$delta]['#attributes']['videoID'] = $matches[3];
          }
          break;

        case 'dailymotion':
          unset($element[$delta]['#attributes']['src']);
          $regexp = '/video\/(.*)$/';
          preg_match($regexp, $value, $matches);
          if (!empty($matches[1])) {
            $element[$delta]['#attributes']['videoID'] = $matches[1];
          }
          break;

        case 'vimeo':
          unset($element[$delta]['#attributes']['src']);
          $regexp = '/vimeo.*\/(\d+)/';
          preg_match($regexp, $value, $matches);
          if (!empty($matches[1])) {
            $element[$delta]['#attributes']['videoID'] = $matches[1];
          }
          break;
      }
    }

    return $element;
  }

}
