<?php

namespace Drupal\tacjs_log\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Datetime\DateFormatter;

/**
 * {@inheritdoc}
 */
class LogController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * The datetime.time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;
  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;
  /**
   * The Date formatter.
   *
   * @var Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a DbLog object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   * @param \Drupal\Component\Datetime\TimeInterface $time_service
   *   The datetime.time service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack object.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The Date formatter.
   */
  public function __construct(Connection $connection, TimeInterface $time_service, RequestStack $request_stack, DateFormatter $date_formatter) {
    $this->connection = $connection;
    $this->timeService = $time_service;
    $this->request = $request_stack->getCurrentRequest();
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Store proof of consent.
   *
   * @param string $service
   *   Unique ID of the service.
   *
   * @return array
   *   Empty Array.
   *
   * @throws \Exception
   */
  public function report($service) {
    try {
      $this->connection
        ->insert('tacjslog')
        ->fields([
          'timestamp' => $this->timeService->getCurrentTime(),
          'ip_address' => $this->request->getClientIp(),
          'services_allowed' => $service,
        ])
        ->execute();
    }
    catch (\Exception $e) {
      throw $e;
    }

    return new JsonResponse([
      'timestamp' => $this->timeService->getCurrentTime(),
      'ip_address' => $this->request->getClientIp(),
      'services_allowed' => $service,
    ]);
  }

  /**
   * Displays a listing of database logs.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   *
   * @throws \Exception
   */
  public function overview() {
    $rows = [];
    $header = [
      [
        'data' => $this->t('Timestamp'),
        'field' => 'log.timestamp',
        'sort' => 'desc',
      ],
      [
        'data' => $this->t('IP address'),
        'field' => 'log.ip_address',
      ],
      [
        'data' => $this->t('Services allowed'),
        'field' => 'log.services_allowed',
      ],
    ];

    try {
      $query = $this->connection
        ->select('tacjslog', 'log')
        ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
        ->extend('\Drupal\Core\Database\Query\TableSortExtender')
        ->fields('log', [
          'timestamp',
          'ip_address',
          'services_allowed',
        ]);

      // Execute the statement.
      $data = $query
        ->limit(50)
        ->orderByHeader($header)
        ->execute();

      // Get all the results.
      $results = $data->fetchAll(\PDO::FETCH_OBJ);
    }
    catch (\Exception $e) {
      throw $e;
    }

    foreach ($results as $result) {
      $rows[] = [
        'data' => [
          $this->dateFormatter->format($result->timestamp, 'short'),
          $result->ip_address,
          $result->services_allowed,
        ],
      ];
    }

    $build['tacjslog_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No log available.'),
    ];

    $build['tacjslog_pager'] = ['#type' => 'pager'];

    return $build;
  }

}
