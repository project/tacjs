tarteaucitron.pro = function (list) {
  "use strict";
  const service = list.match(/!([a-z_]+)=([a-z]+)/i)[1];
  const status = list.match(/!([a-z_]+)=([a-z]+)/i)[2];

  if (status === "engage") {
    fetch(tarteaucitron.baseUrl + "reports/tacjslog/" + service);
  }
};
