<?php

/**
 * Implements hook_tacjs_content_alter().
 */
function hook_tacjs_content_alter(array &$content) {
  $content['analytic']['my_custom_analytic_service'] = [
    "about" => [
      "name" => "My Custom Analytic Service",
    ],
    // Generates warnings in logs if omitted.
    "code" => [
      "js" => "",
      "html" => "",
    ],
  ];
}
