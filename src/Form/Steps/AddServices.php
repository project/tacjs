<?php

namespace Drupal\tacjs\Form\Steps;

use Drupal\Core\Config\Config;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class of AddServices.
 *
 * @package Drupal\tacjs\Form
 */
class AddServices extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The tarteaucitron.js services.
   *
   * @var array
   */
  protected $content;

  /**
   * Constructs a new AddServices.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
    $this->content = $this->getContent();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_add_services';

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tacjs.settings'];
  }

  /**
   * Get configuration.
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected function getConfig() {
    return $this->config('tacjs.settings');
  }

  /**
   * Add elements to form.
   *
   * @param array $form
   * @param \Drupal\Core\Config\Config $config
   *
   * @return void
   */
  protected function addFormElements(array &$form, Config $config) {

    foreach ($this->languageManager->getLanguages() as $key => $value) {
      $langcodes[$key] = $value->getName();
    }

    $form['services'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Add Services'),
      '#title_display' => 'invisible',
    ];

    foreach ($this->content as $type => $services) {
      $form['service']['type_' . $type] = [
        '#type' => 'details',
        '#title' => Unicode::ucfirst($type),
        '#group' => 'services',
      ];

      foreach ($services as $service => $value) {
        $form['service']['type_' . $type]['service_' . $service . '_status'] = [
          '#type' => 'checkbox',
          '#title' => $value['about']['name'],
          '#default_value' => $config->get('services.' . $service . '.status'),
        ];

        $form['service']['type_' . $type]['details_' . $service] = [
          '#type' => 'fieldset',
          '#title' => 'Configuration',
          '#states' => [
            'visible' => [
              ':input[name="service_' . $service . '_status"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form['service']['type_' . $type]['details_' . $service]['service_' . $service . '_languages'] = [
          '#title' => $this->t('Languages'),
          '#type' => 'checkboxes',
          '#options' => $langcodes,
          '#default_value' => $config->get('services.' . $service . '.languages') ?: [],
          '#description' => $this->t('Leave empty to allow any.'),
        ];

        $form['service']['type_' . $type]['details_' . $service]['service_' . $service . '_readMore'] = [
          '#type' => 'textarea',
          '#title' => $this->t('More info'),
          '#default_value' => $config->get('services.' . $service . '.readMore'),
        ];

        $form['service']['type_' . $type]['details_' . $service]['service_' . $service . '_readmoreLink'] = [
          '#type' => 'textfield',
          '#title' => $this->t('More info link'),
          '#default_value' => $config->get('services.' . $service . '.readmoreLink'),
        ];

        $form['service']['type_' . $type]['details_' . $service]['service_' . $service . '_needConsent'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Need consent'),
          '#default_value' => $config->get('services.' . $service . '.needConsent'),
        ];

        if (isset($value['code'])) {
          foreach ($value['code'] as $code) {
            if (preg_match_all('/tarteaucitron.user.([A-z]+)[\s]+=[\s]+[\\]?[\"]?[\']?###([^.]+)###/m', (is_array($code) ? implode(", ", $code) : $code), $match)) {
              for ($i = 0; $i < count($match[1]); $i++) {
                $form['service']['type_' . $type]['details_' . $service]['user_' . $service . '_' . $match[1][$i]] = [
                  '#type' => 'textfield',
                  '#title' => $match[1][$i],
                  '#default_value' => $config->get('user.' . $match[1][$i]),
                  '#title_display' => 'invisible',
                  '#maxlength' => 255,
                  '#attributes' => [
                    'placeholder' => $match[2][$i],
                  ],
                ];
              }
            }
          }
        }
      }
    }

    // Workaround for #3170835: Uncaught TypeError null user
    // See https://github.com/AmauriC/tarteaucitron.js/pull/394
    $form['service']['type_analytic']['user_multiplegtagUa']['#attributes']['placeholder'] = 'UA-XXXXXXXX-X, UA-XXXXXXXX-X, UA-XXXXXXXX-X';

    // Workaround for #3204238: Uncaught TypeError null user
    // See https://www.drupal.org/project/tacjs/issues/3204238
    $form['service']['type_api']['user_googleFonts']['#attributes']['placeholder'] = 'My Font, My Other:300,700';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $this->addFormElements($form, $config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->content as $services) {
      foreach ($services as $service => $value) {
        $form_state->setValue('service_' . $service . '_status', $form_state->getValue('service_' . $service . '_status') ? TRUE : FALSE);
        $form_state->setValue('service_' . $service . '_needConsent', $form_state->getValue('service_' . $service . '_needConsent') ? TRUE : FALSE);
        // Filter out zero numeric values from languages array as schema requires a unique type: string.
        $form_state->setValue('service_' . $service . '_languages', array_filter($form_state->getValue('service_' . $service . '_languages')));

        if (isset($value['code'])) {
          foreach ($value['code'] as $code) {
            if (preg_match_all('/tarteaucitron.user.([A-z]+)[\s]+=[\s]+[\\]?[\"]?[\']?###([^.]+)###/m', (is_array($code) ? implode(", ", $code) : $code), $match)) {
              for ($i = 0; $i < count($match[1]); $i++) {
                $form_state->setValue('user_' . $service . '_' . $match[1][$i], $form_state->getValue('user_' . $service . '_' . $match[1][$i]) ?: NULL);
              }
            }
          }
        }
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * Process submitted configuration.
   */
  protected function processSubmit(FormStateInterface $form_state, Config $config) {

    $userSettings = [];

    foreach ($this->content as $services) {
      foreach ($services as $service => $value) {
        $services_array = [
          "status" => $form_state->getValue('service_' . $service . '_status'),
          "languages" => $form_state->getValue('service_' . $service . '_languages'),
          "readMore" => $form_state->getValue('service_' . $service . '_readMore'),
          "readmoreLink" => $form_state->getValue('service_' . $service . '_readmoreLink'),
          "needConsent" => $form_state->getValue('service_' . $service . '_needConsent'),
        ];

        $config->set('services.' . $service, $services_array);

        if (isset($value['code'])) {
          foreach ($value['code'] as $code) {
            if (preg_match_all('/tarteaucitron.user.([A-z]+)[\s]+=[\s]+[\\]?[\"]?[\']?###([^.]+)###/m', (is_array($code) ? implode(", ", $code) : $code), $match)) {
              for ($i = 0; $i < count($match[1]); $i++) {
                if ($form_state->getValue('user_' . $service . '_' . $match[1][$i])) {
                  $config->set('user.' . $match[1][$i], $form_state->getValue('user_' . $service . '_' . $match[1][$i]));
                  $userSettings[] = 'user.' . $match[1][$i];
                }
                elseif (!in_array('user.' . $match[1][$i], $userSettings)) {
                  $config->clear('user.' . $match[1][$i]);
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $this->processSubmit($form_state, $config);

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function for the tacjs.add_services form.
   *
   * Default tarteaucitron.js services.
   *
   * You can copy content directly from the following URL:
   * https://opt-out.ferank.eu/json.php
   *
   * @return array
   *   Info array.
   */
  protected function getContent() {
    // Get the module base path.
    $relative_path = dirname(__DIR__, 3);

    // Get the content.json file from the base path.
    $content_json_path = $relative_path . '/assets/data/content.json';

    // Load file content.
    $content_json = file_get_contents($content_json_path);

    $content = Json::decode($content_json);

    // Allow modules to inject custom services into settings form.
    $this->moduleHandler->alter('tacjs_content', $content);

    return $content;
  }

}
