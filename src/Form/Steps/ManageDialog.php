<?php

namespace Drupal\tacjs\Form\Steps;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class of ManageDialog.
 *
 * @package Drupal\tacjs\Form
 */
class ManageDialog extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_manage_dialog';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tacjs.settings'];
  }

  /**
   * Get configuration.
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected function getConfig() {
    return $this->config('tacjs.settings');
  }

  /**
   * Add elements to form.
   *
   * @param array $form
   * @param \Drupal\Core\Config\Config $config
   *
   * @return void
   */
  protected function addFormElements(array &$form, Config $config) {

    $form['privacyUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Privacy url'),
      '#description' => $this->t('Privacy policy url'),
      '#default_value' => $config->get('dialog.privacyUrl'),
    ];

    $form['bodyPosition'] = [
      '#type' => 'radios',
      '#title' => $this->t('Body position'),
      '#description' => $this->t('or top to bring it as first element for accessibility'),
      '#default_value' => $config->get('dialog.bodyPosition'),
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
    ];

    $form['hashtag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hashtag'),
      '#description' => $this->t('Open the panel with this hashtag'),
      '#default_value' => $config->get('dialog.hashtag'),
    ];

    $form['cookieName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie name'),
      '#default_value' => $config->get('dialog.cookieName'),
      '#required' => TRUE,
    ];

    $form['orientation'] = [
      '#type' => 'radios',
      '#title' => $this->t('Orientation'),
      '#description' => $this->t('Banner position'),
      '#default_value' => $config->get('dialog.orientation'),
      '#options' => [
        'top' => $this->t('Top'),
        'middle' => $this->t('Middle'),
        'bottom' => $this->t('Bottom'),
      ],
    ];

    $form['groupServices'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Groupe Services'),
      '#description' => $this->t('Group services by category'),
      '#default_value' => $config->get('dialog.groupServices'),
    ];

    $form['serviceDefaultState'] = [
      '#type' => 'radios',
      '#title' => $this->t('Default state'),
      '#default_value' => $config->get('dialog.serviceDefaultState'),
      '#options' => [
        'true' => $this->t('True'),
        'wait' => $this->t('Wait'),
        'false' => $this->t('False'),
      ],
    ];

    $form['showAlertSmall'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show alert small'),
      '#description' => $this->t('Show the small banner on bottom right'),
      '#default_value' => $config->get('dialog.showAlertSmall'),
    ];

    $form['cookieslist'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cookies list'),
      '#description' => $this->t('Show the cookie list'),
      '#default_value' => $config->get('dialog.cookieslist'),
    ];

    $form['showIcon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Icon'),
      '#description' => $this->t('Show cookie icon to manage cookies'),
      '#default_value' => $config->get('dialog.showIcon'),
    ];

    $form['iconSrc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Icon source'),
      '#description' => $this->t('URL or base64 encoded image'),
      '#default_value' => $config->get('dialog.iconSrc'),
    ];

    $form['iconPosition'] = [
      '#type' => 'radios',
      '#title' => $this->t('Icon position'),
      '#description' => $this->t('Position of the icon'),
      '#default_value' => $config->get('dialog.iconPosition'),
      '#options' => [
        'BottomRight' => $this->t('BottomRight'),
        'BottomLeft' => $this->t('BottomLeft'),
        'TopRight' => $this->t('TopRight'),
        'TopLeft' => $this->t('TopLeft'),
      ],
    ];

    $form['adblocker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Adblocker'),
      '#description' => $this->t('Show a Warning if an adblocker is detected'),
      '#default_value' => $config->get('dialog.adblocker'),
    ];

    $form['DenyAllCta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Deny all cta'),
      '#description' => $this->t('Show the deny all button'),
      '#default_value' => $config->get('dialog.DenyAllCta'),
    ];

    $form['AcceptAllCta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Accept all cta'),
      '#description' => $this->t('Show the accept all button when highPrivacy on'),
      '#default_value' => $config->get('dialog.AcceptAllCta'),
    ];

    $form['highPrivacy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('High privacy'),
      '#description' => $this->t('HIGHLY RECOMMANDED Disable auto consent'),
      '#default_value' => $config->get('dialog.highPrivacy'),
    ];

    $form['handleBrowserDNTRequest'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Handle browser DNT request'),
      '#description' => $this->t('If Do Not Track == 1, disallow all'),
      '#default_value' => $config->get('dialog.handleBrowserDNTRequest'),
    ];

    $form['removeCredit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove credit'),
      '#description' => $this->t('Remove credit link'),
      '#default_value' => $config->get('dialog.removeCredit'),
    ];

    $form['moreInfoLink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('More info link'),
      '#description' => $this->t('Show more info link'),
      '#default_value' => $config->get('dialog.moreInfoLink'),
    ];

    $form['useExternalCss'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use external CSS'),
      '#description' => $this->t('If false, the tarteaucitron.css file will be loaded'),
      '#default_value' => $config->get('dialog.useExternalCss'),
    ];

    $form['useExternalJs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use external JS'),
      '#description' => $this->t('If false, the tarteaucitron.services.js file will be loaded'),
      '#default_value' => $config->get('dialog.useExternalJs'),
    ];

    $form['cookieDomain'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cookie domain'),
      '#description' => $this->t('Shared cookie for subdomain website'),
      '#default_value' => $config->get('dialog.cookieDomain'),
    ];

    $form['readmoreLink'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Read more link'),
      '#description' => $this->t('Change the default readmore link pointing to tarteaucitron.io'),
      '#default_value' => $config->get('dialog.readmoreLink'),
    ];

    $form['mandatory'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mandatory'),
      '#description' => $this->t('Show a message about mandatory cookies'),
      '#default_value' => $config->get('dialog.mandatory'),
    ];

    $form['mandatoryCta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('mandatoryCta'),
      '#description' => $this->t('Show the disabled accept button when mandatory on'),
      '#default_value' => $config->get('dialog.mandatoryCta'),
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
    ];

    $form['advanced']['closePopup'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Close Popup'),
      '#default_value' => $config->get('dialog.closePopup'),
    ];

    $form['advanced']['customCloserId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom closer ID'),
      '#default_value' => $config->get('dialog.customCloserId'),
    ];

    $form['advanced']['expire'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiration time'),
      '#default_value' => $config->get('expire'),
      '#field_suffix' => ' ' . $this->t('day(s)'),
      '#min' => 1,
      '#max' => 365,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $this->addFormElements($form, $config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state
      ->setValue('groupServices', $form_state->getValue('groupServices') ? TRUE : FALSE)
      ->setValue('showAlertSmall', $form_state->getValue('showAlertSmall') ? TRUE : FALSE)
      ->setValue('cookieslist', $form_state->getValue('cookieslist') ? TRUE : FALSE)
      ->setValue('showIcon', $form_state->getValue('showIcon') ? TRUE : FALSE)
      ->setValue('adblocker', $form_state->getValue('adblocker') ? TRUE : FALSE)
      ->setValue('DenyAllCta', $form_state->getValue('DenyAllCta') ? TRUE : FALSE)
      ->setValue('AcceptAllCta', $form_state->getValue('AcceptAllCta') ? TRUE : FALSE)
      ->setValue('highPrivacy', $form_state->getValue('highPrivacy') ? TRUE : FALSE)
      ->setValue('handleBrowserDNTRequest', $form_state->getValue('handleBrowserDNTRequest') ? TRUE : FALSE)
      ->setValue('removeCredit', $form_state->getValue('removeCredit') ? TRUE : FALSE)
      ->setValue('moreInfoLink', $form_state->getValue('moreInfoLink') ? TRUE : FALSE)
      ->setValue('useExternalCss', $form_state->getValue('useExternalCss') ? TRUE : FALSE)
      ->setValue('useExternalJs', $form_state->getValue('useExternalJs') ? TRUE : FALSE)
      ->setValue('mandatory', $form_state->getValue('mandatory') ? TRUE : FALSE)
      ->setValue('closePopup', $form_state->getValue('closePopup') ? TRUE : FALSE)
      ->setValue('expire', (int) $form_state->getValue('expire') ?: NULL);

    parent::validateForm($form, $form_state);
  }

  /**
   * Process submitted configuration.
   */
  protected function processSubmit(FormStateInterface $form_state, Config $config) {

    $config
      ->set('dialog.privacyUrl', $form_state->getValue('privacyUrl'))
      ->set('dialog.bodyPosition', $form_state->getValue('bodyPosition'))
      ->set('dialog.hashtag', $form_state->getValue('hashtag'))
      ->set('dialog.cookieName', $form_state->getValue('cookieName'))
      ->set('dialog.orientation', $form_state->getValue('orientation'))
      ->set('dialog.groupServices', $form_state->getValue('groupServices'))
      ->set('dialog.serviceDefaultState', $form_state->getValue('serviceDefaultState'))
      ->set('dialog.showAlertSmall', $form_state->getValue('showAlertSmall'))
      ->set('dialog.cookieslist', $form_state->getValue('cookieslist'))
      ->set('dialog.showIcon', $form_state->getValue('showIcon'))
      ->set('dialog.iconSrc', $form_state->getValue('iconSrc'))
      ->set('dialog.iconPosition', $form_state->getValue('iconPosition'))
      ->set('dialog.adblocker', $form_state->getValue('adblocker'))
      ->set('dialog.DenyAllCta', $form_state->getValue('DenyAllCta'))
      ->set('dialog.AcceptAllCta', $form_state->getValue('AcceptAllCta'))
      ->set('dialog.highPrivacy', $form_state->getValue('highPrivacy'))
      ->set('dialog.handleBrowserDNTRequest', $form_state->getValue('handleBrowserDNTRequest'))
      ->set('dialog.removeCredit', $form_state->getValue('removeCredit'))
      ->set('dialog.moreInfoLink', $form_state->getValue('moreInfoLink'))
      ->set('dialog.useExternalCss', $form_state->getValue('useExternalCss'))
      ->set('dialog.useExternalJs', $form_state->getValue('useExternalJs'))
      ->set('dialog.cookieDomain', $form_state->getValue('cookieDomain'))
      ->set('dialog.readmoreLink', $form_state->getValue('readmoreLink'))
      ->set('dialog.mandatory', $form_state->getValue('mandatory'))
      ->set('dialog.mandatoryCta', $form_state->getValue('mandatoryCta'))
      ->set('dialog.closePopup', $form_state->getValue('closePopup'))
      ->set('dialog.customCloserId', $form_state->getValue('customCloserId'))
      ->set('expire', $form_state->getValue('expire'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $this->processSubmit($form_state, $config);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
